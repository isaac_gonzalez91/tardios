var app = angular.module("reporteTardiosApp", []);

app.controller("OrganizacionController", ["$http", "$log", function($http, $log) {

      var organizacion = this;
      var log = $log;

      organizacion.hasta = new Date();
      organizacion.desde = new Date();

      organizacion.turnoSeleccionado = null;
      organizacion.departamentoSeleccionado = null;
      organizacion.checadorSeleccionado = null;

      organizacion.empleadosSeleccionados = [];

      // arreglos vacios
      organizacion.turnos = [];
      organizacion.empleados = [];
      organizacion.departamentos = [];
      organizacion.checadores = [];

      organizacion.monitoreados = false;

      // los llenamos mediante el API
      organizacion.turnos = [
        {id : 1, nombre : "turno 1", empleados : [ 1, 2 ]},
        {id : 2, nombre : "turno 2", empleados : [ 2, 3 ]}
      ];

      organizacion.empleados = [
        {id : 1, nombre : "Isaac", monitoreado : false},
        {id : 2, nombre : "David", monitoreado : false},
        {id : 3, nombre : "Dany", monitoreado: false}
      ];

      organizacion.departamentos = [
        {id : 1, nombre : "General", empleados : [ 1 ]},
        {id : 2, nombre : "Pruebas", empleados : [ 2 ]},
        {id : 3, nombre : "Desarrollo", empleados : [ 3 ]}
      ];

      organizacion.checadores = [
        {id : 1, nombre : "Entrada", empleados : [ 1, 2, 3 ]},
        {id : 2, nombre : "Salida A", empleados : [ 1, 3 ]},
        {id : 3, nombre : "Salida B", empleados : [ 1, 2 ]}
      ];

      organizacion.asistencias = [
        { id : 1, empleado : 1, fecha : new Date() },
        { id : 2, empleado : 2, fecha : new Date() },
        { id : 3, empleado : 3, fecha : new Date() },
        { id : 4, empleado : 2, fecha : new Date() },
        { id : 5, empleado : 1, fecha : new Date() },
        { id : 6, empleado : 3, fecha : new Date() },
      ];

      $http.get(
          "http://192.168.1.106:8080/SASB/sincronizacion/exposeData?correo=zaida.burrola@inercy.com&direccionMac=00:bb:3a:77:43:b6",
          {organizacion : 1})
       .then(function(data) {
           // console.log(data);
       });

       organizacion.getEmpleado = function(id){
           for (var i = 0; i < organizacion.empleados.length; i++) {
               if(organizacion.empleados[i].id == id)
                 return organizacion.empleados[i];
           }
       }

      organizacion.empleadosFiltrados = function() {

          var o = organizacion;

          var empleados = []

          // Sin nada
          if (o.turnoSeleccionado == null &&
              o.departamentoSeleccionado == null  &&
              o.checadorSeleccionado == null ) {

              var e = [];

              for (empleado of o.empleados) {
                    if(empleado.monitoreado === o.monitoreados){
                        e.push(empleado);
                    }
              }

              return e;
          }

          // Validaciones para turno

          // turno solito
          if (o.turnoSeleccionado != null &&
              o.departamentoSeleccionado == null  &&
              o.checadorSeleccionado == null) {
                empleados = getEmpleadosInArray(o.empleados, o.turnoSeleccionado, o.monitoreados);
          }

          // turno y departamento
          if (o.turnoSeleccionado != null &&
              o.departamentoSeleccionado != null  &&
              o.checadorSeleccionado == null) {
                empleados = _.intersection(
                    getEmpleadosInArray(o.empleados, o.turnoSeleccionado, o.monitoreados),
                    getEmpleadosInArray(o.empleados, o.departamentoSeleccionado, o.monitoreados)
                );
          }

          // turno y checador
          if (o.turnoSeleccionado != null &&
              o.departamentoSeleccionado == null  &&
              o.checadorSeleccionado != null) {
                empleados = _.intersection(
                    getEmpleadosInArray(o.empleados, o.turnoSeleccionado, o.monitoreados),
                    getEmpleadosInArray(o.empleados, o.checadorSeleccionado, o.monitoreados)
                );
          }

          // turno, departamento y checador
          if (o.turnoSeleccionado != null &&
              o.departamentoSeleccionado != null  &&
              o.checadorSeleccionado != null) {
                empleados = _.intersection(
                    getEmpleadosInArray(o.empleados, o.turnoSeleccionado, o.monitoreados),
                    getEmpleadosInArray(o.empleados, o.departamentoSeleccionado, o.monitoreados),
                    getEmpleadosInArray(o.empleados, o.checadorSeleccionado, o.monitoreados)
                );
          }

          // validaciones para departamento

          // departamento solito
          if (o.turnoSeleccionado == null &&
                o.departamentoSeleccionado != null  &&
                o.checadorSeleccionado == null) {
                empleados = getEmpleadosInArray(o.empleados, o.departamentoSeleccionado);
          }

          // departamento y checador
          if (o.turnoSeleccionado == null &&
              o.departamentoSeleccionado != null  &&
              o.checadorSeleccionado != null) {
                empleados = _.intersection(
                    getEmpleadosInArray(o.empleados, o.departamentoSeleccionado, o.monitoreados),
                    getEmpleadosInArray(o.empleados, o.checadorSeleccionado, o.monitoreados)
                );
          }


        // validaciones para checador

        // checador solito
        if (o.turnoSeleccionado == null &&
              o.departamentoSeleccionado == null  &&
              o.checadorSeleccionado != null) {
              empleados = getEmpleadosInArray(o.empleados, o.checadorSeleccionado);
        }

        return empleados;
      }

      function getEmpleadosInArray(empleados, array, monitoreado){
          var e = []

          for (var i = 0; i < empleados.length; i++) {
              for (var j = 0; j < array.empleados.length; j++) {
                  if (empleados[i].id === array.empleados[j]) {
                      if(!monitoreado){
                        e.push(empleados[i]);
                      }
                  }
              }
          }

          return e;
      }

      organizacion.getAsistencias = function(){

          var a = [];

          for (asistencia of organizacion.asistencias) {

              if(asistencia.fecha >= organizacion.desde &&
                  asistencia.fecha <= organizacion.hasta){

                      for(empleado of organizacion.empleadosSeleccionados){

                          if(empleado.id === asistencia.empleado){
                              a.push(asistencia)
                          }

                      }


              }
          }

          return a;
      }

}]);
